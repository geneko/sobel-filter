from __future__ import division
import ctypes
import sys
import time

import multiprocessing as mp
import numpy as np

# Create a global shared object as a module for multiprocessing
# This is required for multiprocessing on Windows
# http://stackoverflow.com/questions/1675766/how-to-combine-pool-map-with-array-shared-memory-in-python-multiprocessing
sharedObject = sys.modules[__name__]

def sobel(neighbor):
    """
    Applies a Sobel filter to a pixel using its 3x3 surrounding neighborhood

    Parameters:
    -----------
    neighbor: <NumPy array>, shape=(3, 3), dtype=np.float
        * A 3x3 image neighborhood

    Returns:
    -------
    g: float
        * Pixel value after applying Sobel filter
    """
    # Define Sobel kernels (Rotated 180 degrees)
    kernelX = np.array([[1, 0, -1], [2, 0, -2], [1, 0, -1]], dtype=np.int)
    kernelY = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]], dtype=np.int)

    # Apply sobel filter (elementwise multiplication)
    gX = (kernelX*neighbor).sum()
    gY = (kernelY*neighbor).sum()
    g = np.sqrt(gX**2 + gY**2)

    return g
    
def scale(matrix):
    """
    Scales image matrix to values [0,1]

    Parameters:
    -----------
    matrix: <NumPy array>, shape(x, y), dtype=np.float
        * Image matrix after applying sobel filter

    Returns:
    --------
    matrix: <NumPy array>, shape=(x, y), dtype=np.float
        * Normalized image matrix.
    """
    # Check input type before proceeding.
    matrixMin = matrix.min()
    matrixMax = matrix.max()
    matrix = (matrix - matrixMin)/(matrixMax - matrixMin)

    return matrix

def parJob(args):
    """
    This is the parallelized Sobel filter. Each multiprocessing jobserver 
    applies the Sobel filter to a single row of the image.

    Parameters:
    -----------
    i: <int>
        * Row number of image to apply Sobel filter.

    img: <NumPy array>, shape=(ypixels, xpixels), range=[0, 1], dtype=np.float
        * Pixel values of image before applying sobel filter
    """

    i, nPixelsX, nPixelsY = args
   
    # Point img and fimg to shared arrays in sharedObject and convert into
    # workable NumPy shape.
    s_img = np.frombuffer(sharedObject.c_img)
    s_imgF = np.frombuffer(sharedObject.c_imgF)
    img = s_img.reshape((nPixelsX, nPixelsY))
    imgF = s_imgF.reshape((nPixelsX, nPixelsY))
    
    neighbor = np.zeros((3, 3))

    for j in range(0, nPixelsY):
        # Extract image neighborhood
        for inX in range(0, 3):
            for inY in range(0, 3):
                posX = i - 1 + inX
                posY = j - 1 + inY
                if 0 <= posY < nPixelsY and 0 <= posX < nPixelsX:
                    neighbor[inX, inY] = img[posX, posY]
                else:
                    neighbor[inX, inY] = 0
                    
        # Sobel Filter
        imgF[i, j] = sobel(neighbor)
    
def sharedInit(dictShared):
    for sharedName, sharedValue in dictShared.iteritems(): 
        setattr(sharedObject, sharedName, sharedValue)

# apply() is a deprecated Python built-in function(), so we can use it.
def apply(img, nJobs=1):
    """
    Function to apply sobel filter.

    Parameters:
    -----------
    img: <NumPy array>, shape=(ypixels, xpixels), range=[0, 1], dtype=np.float
        * Pixel values of image before applying sobel filter
    
    nJobs: <int>
        * The number of jobs to run in parallel. If n_jobs=-1, then the number
          of jobs is set to the number of detected CPU cores.

    Returns:
    -------
    imgF: <NumPy array>, shape=(ypixels, xpixels), range=[0, 1], dtype=np.float
        * PIxel values of image after applying sobel filter
    """
    timeStart = time.time()

    # Initialize a matrix to store filtered image
    # See https://github.com/scikit-learn/scikit-learn/wiki/C-integer-types:-the-missing-manual
    # RE: npnpy_intp type
    nPixelsX = img.shape[0]
    nPixelsY = img.shape[1]
    
    # Serial Sobel filter
    if nJobs == 1:
        imgF = np.zeros((nPixelsX, nPixelsY), dtype=np.float)
        neighbor = np.zeros((3, 3), dtype=np.float)
        for i in range(0, nPixelsX):
            for j in range(0, nPixelsY):
                # Extract image neighborhood
                for inX in range(0, 3):
                    for inY in range(0, 3):
                        posX = i - 1 + inX
                        posY = j - 1 + inY
                        if 0 <= posY < nPixelsY and 0 <= posX < nPixelsX:
                            neighbor[inX, inY] = img[posX, posY]
                        else:
                            neighbor[inX, inY] = 0
                # Sobel Filter
                imgF[i, j] = sobel(neighbor)
                
    # Parallel Sobel Filter
    else:
        # Create shared arrays. We can disable locking because s_c_img
        # is a read-only array and no process will ever write to the same
        # memory location in s_c_imgF.
        #
        # Benchmarks show that the use of shared memory results in a
        # significant reduction in overall memory consumption due to no
        # passing of the large image matrices, but may result in greater
        # execution time due to the overhead.
        s_c_img = mp.Array(ctypes.c_double, nPixelsX*nPixelsY, lock=False)
        s_c_imgF = mp.Array(ctypes.c_double, nPixelsX*nPixelsY, lock=False)

        # Populate shared memory object with pixel values from img
        s_img = np.frombuffer(s_c_img)
        s_img2D = s_img.reshape((nPixelsX, nPixelsY))
        s_img2D[:] = img
        
        dictShared = {'c_img': s_c_img, 'c_imgF': s_c_imgF}
        pool = mp.Pool(nJobs, initializer=sharedInit, initargs=(dictShared,))

        params = zip(range(0, nPixelsX), nPixelsX*[nPixelsX], nPixelsX*[nPixelsY])
        pool.map(parJob, params)
        pool.close()
        pool.join()
        
        shared_fimg = np.frombuffer(s_c_imgF)
        imgF = shared_fimg.reshape((nPixelsX, nPixelsY))
        
    # Rescale Sobel processed image
    imgF = scale(imgF)
    
    timeEnd = time.time()
    timeKernel = (timeEnd - timeStart)  # Seconds
    
    return imgF, timeKernel