from __future__ import division
import ctypes
import sys
import time

import multiprocessing as mp
import numpy as np
cimport numpy as np

# Create a global shared object as a module for multiprocessing
# This is required for multiprocessing on Windows
# http://stackoverflow.com/questions/1675766/how-to-combine-pool-map-with-array-shared-memory-in-python-multiprocessing
cdef object sharedObject = sys.modules[__name__]

def sobel(np.ndarray[double, ndim=2] neighbor):
    """
    Applies a Sobel filter to a pixel using its 3x3 surrounding neighborhood

    Parameters:
    -----------
    neighbor: <NumPy array>, shape=(3, 3), dtype=np.float
        * A 3x3 image neighborhood

    Returns:
    -------
    g: float
        * Pixel value after applying Sobel filter
    """
    # Check input type before proceeding.
    assert neighbor.dtype == np.float

    # Define Sobel kernels (rotated 180 degrees)
    cdef np.ndarray[int, ndim=2] kernelX
    cdef np.ndarray[int, ndim=2] kernelY
    kernelX = np.array([[1, 0, -1], [2, 0, -2], [1, 0, -1]], dtype=np.int)
    kernelY = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]], dtype=np.int)

    # Apply sobel filter by elementwise multiplication
    # This is faster than the equivalent (kernel*neighbor).sum()
    cdef double g, gX, gY
    gX = 0
    gY = 0
    for i in range(0, 3):
        for j in range(0, 3):
            gX += kernelX[i, j]*neighbor[i, j]
            gY += kernelY[i, j]*neighbor[i, j]
    g = np.sqrt(gX**2 + gY**2)

    return g
    
def scale(np.ndarray[double, ndim=2] matrix):
    """
    Scales image matrix to values [0,1]

    Parameters:
    -----------
    matrix: <NumPy array>, shape(x, y), dtype=np.float
        * Image matrix after applying sobel filter

    Returns:
    --------
    matrix: <NumPy array>, shape=(x, y), dtype=np.float
        * Normalized image matrix [0, 1]
    """
    # Check input type before proceeding.
    assert matrix.dtype == np.float

    cdef double matrixMin = matrix.min()
    cdef double matrixMax = matrix.max()
    matrix = (matrix - matrixMin)/(matrixMax - matrixMin)

    return matrix

   
def parJob(tuple args):
    """
    This is the parallelized Sobel filter. Each multiprocessing jobserver 
    applies the Sobel filter to a single row of the image.

    Parameters:
    -----------
    i: <int>
        * Row number of image to apply Sobel filter.

    img: <NumPy array>, shape=(ypixels, xpixels), range=[0, 1], dtype=np.float
        * Pixel values of image before applying sobel filter
    
    nPixelsX: <int>
        * Number of rows in the image matrix
    
    nPixelsY: <int>
        * Number of columns in the image matrix
    """

    cdef int i, nPixelsX, nPixelsY
    i, nPixelsX, nPixelsY = args
   
    # Point img and fimg to shared arrays in sharedObject and convert into
    # workable NumPy shape.
    cdef np.ndarray[double, ndim=1] s_img, s_imgF
    cdef np.ndarray[double, ndim=2] img, imgF
    s_img = np.frombuffer(sharedObject.c_img)
    s_imgF = np.frombuffer(sharedObject.c_imgF)
    img = s_img.reshape((nPixelsX, nPixelsY))
    imgF = s_imgF.reshape((nPixelsX, nPixelsY))
    
    cdef np.ndarray[double, ndim=2] neighbor
    neighbor = np.zeros((3, 3))

    cdef int j
    for j in range(0, nPixelsY):
        # Extract image neighborhood
        for inX in range(0, 3):
            for inY in range(0, 3):
                posX = i - 1 + inX
                posY = j - 1 + inY
                if 0 <= posY < nPixelsY and 0 <= posX < nPixelsX:
                    neighbor[inX, inY] = img[posX, posY]
                else:
                    neighbor[inX, inY] = 0
                    
        # Sobel Filter
        imgF[i, j] = sobel(neighbor)

def sharedInit(dict dictShared):
    """
    The purpose of this function is to allow the multiprocessing Pool() to
    initialize with your desired shared arrays so it can operate under Windows.
    
    We created a dummy global shared module named sharedObject at the top of
    this file. The purpose of sharedObject is to store the shared arrays
    as its attributes.
    
    Parameters:
    -----------
    dictShared: <dict>, {sharedMemoryArray: attributeName}
        * 
    """
    
    for sharedName, sharedValue in dictShared.iteritems(): 
        setattr(sharedObject, sharedName, sharedValue)

# apply() is a deprecated Python built-in function(), so we can use it.
def apply(np.ndarray[double, ndim=2] img, int nJobs=1):
    """
    Function to apply sobel filter.

    Parameters:
    -----------
    img: <NumPy array>, shape=(ypixels, xpixels), range=[0, 1], dtype=np.float
        * Pixel values of image before applying sobel filter
    
    nJobs: <int>
        * The number of jobs to run in parallel. If n_jobs=-1, then the number
          of jobs is set to the number of detected CPU cores.

    Returns:
    -------
    imgF: <NumPy array>, shape=(ypixels, xpixels), range=[0, 1], dtype=np.float
        * PIxel values of image after applying sobel filter
    """
    # Check input type before proceeding
    assert img.dtype == np.float

    cdef double timeStart, timeEnd
    timeStart = time.time()

    # Initialize a matrix to store filtered image
    # See https://github.com/scikit-learn/scikit-learn/wiki/C-integer-types:-the-missing-manual
    # RE: npnpy_intp type
    cdef np.npy_intp nPixelsX, nPixelsY

    nPixelsX = img.shape[0]
    nPixelsY = img.shape[1]
    

    cdef int i, j, inX, inY, posX, posY
    cdef np.ndarray[double, ndim=2] neighbor, imgF, s_img2D
    cdef np.ndarray[double, ndim=1] rowF, s_img, s_imgF
    cdef list parResults
    cdef object s_cimg, s_cimgF, pool
    cdef dict dictShared
    
    # Serial Sobel filter
    if nJobs == 1:
        imgF = np.zeros((nPixelsX, nPixelsY), dtype=np.float)
        neighbor = np.zeros((3, 3), dtype=np.float)
        for i in range(0, nPixelsX):
            for j in range(0, nPixelsY):
                # Extract image neighborhood
                for inX in range(0, 3):
                    for inY in range(0, 3):
                        posX = i - 1 + inX
                        posY = j - 1 + inY
                        if 0 <= posY < nPixelsY and 0 <= posX < nPixelsX:
                            neighbor[inX, inY] = img[posX, posY]
                        else:
                            neighbor[inX, inY] = 0
                # Sobel Filter
                imgF[i, j] = sobel(neighbor)
                
    # Parallel Sobel Filter
    else:
        # Create shared arrays. We can disable locking because s_c_img
        # is a read-only array and no process will ever write to the same
        # memory location in s_c_imgF.
        #
        # Benchmarks show that the use of shared memory results in a small
        # increase in execution time due to the overhead, but also has
        # the added benefit of reduced memory consumption.
        s_c_img = mp.Array(ctypes.c_double, nPixelsX * nPixelsY, lock=False)
        s_c_imgF = mp.Array(ctypes.c_double, nPixelsX * nPixelsY, lock=False)

        # Populate shared memory object with pixel values from img
        s_img = np.frombuffer(s_c_img)
        s_img2D = s_img.reshape((nPixelsX, nPixelsY))
        s_img2D[:] = img
        
        dictShared = {'c_img': s_c_img, 'c_imgF': s_c_imgF}
        pool = mp.Pool(nJobs, initializer=sharedInit, initargs=(dictShared,))

        params = zip(range(0, nPixelsX), nPixelsX * [nPixelsX], nPixelsX * [nPixelsY])
        pool.map(parJob, params)
        pool.close()
        pool.join()
        
        s_imgF = np.frombuffer(s_c_imgF)
        imgF = s_imgF.reshape((nPixelsX, nPixelsY))
                   
    # Rescale Sobel processed image
    imgF = scale(imgF)

    timeEnd = time.time()
    timeKernel = (timeEnd - timeStart)
    
    return imgF, timeKernel