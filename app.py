from __future__ import division # http://www.python.org/dev/peps/pep-0238/

from datetime import datetime
import getopt
import multiprocessing as mp
import sys
import time

from PIL import Image	# Pillow Library in place of old Python Imaging Library
import numpy as np

def usage():
    output = 'Usage: %s -i <input> -o <output> -e <engine> -n <threads>' %sys.argv[0]
    output += '\n -i: Input filename'
    output += '\n -o: Output filename'
    output += '\n -e: Computation engine. Options are ''cuda'', ''opencl-gpu'', ''opencl-cpu'', ''cython'', ''python''. Default is opencl-gpu'
    output += '\n -n: Number of threads. For Cython and Python engines, this number should not exceed the number of CPU cores.'
    output += '\n     For CUDA engine, this is the number of threads per block. This should be in multiples of 32.'
    output += '\n     This parameter is ignored for the OpenCL engine and is set by the PyOpenCL library.'
    print output

def main():
    # Check for minimum number of parameters and that there are no odd number
    # of parameters.
    paramsMin = 2
    if len(sys.argv[1:]) < (paramsMin) or len(sys.argv[1:])%2 != 0:
        usage()
        sys.exit()
    else:
        for i, param in enumerate(sys.argv[1:]):
            if i%2 != 0:
                if str(sys.argv[i][0]) != '-':
                    usage()
                    sys.exit()
    
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'i:o:e:d:n:')
    except getopt.GetoptError:
        usage()
        sys.exit()

    for opt, arg in opts:
        if opt == '-i':
            fnameInput = arg
        elif opt == '-o':
            fnameOutput = arg
        elif opt == '-e':
            engine = str(arg).lower()
            if engine not in ['cuda', 'opencl-gpu', 'opencl-cpu', 'cython', 'python']:
                print 'Error: Parameter -e incorrect.'
                usage()
                sys.exit()
        elif opt == '-n':
            nThreads = arg
            try:
                nThreads = int(nThreads)
            except:
                print 'Error: Parameter -n must be a number.'
                usage()
                sys.exit()
#        elif opt == '-v':
#            verbose = arg
#            if verbose.lower() not in ['0', '1', 'true', 'false']:
#                usage()
#                print 'Error: Parameter -v incorrect.'
#                usage()
#                sys.exit()
#            else:
#                if verbose.lower() in ['1', 'true']:
#                    verbose = True
#                elif verbose.lower in ['0', 'false']:
#                    verbose = False
                        
    # Check nThreads parameter to ensure it is used with engine or within acceptable bounds
    try:
        nThreads
        try:
            engine
        except NameError:
            print 'Error: Parameter -n must be used with -e.'
            usage()
            sys.exit()
        # If engine exists, check to make sure nThreads is within bounds.
        if engine == 'cuda':
            if nThreads < 1 or nThreads > 1024:
                print 'Error: Parameter -n must be a number between 1 and 1024 for CUDA engine.'
                usage()
                sys.exit()
        elif engine == 'opencl-gpu':
            if nThreads < 0 or nThreads > 256:
                print 'Error: Parameter -n must be a number between 1 and 256 for OpenCL GPU engine.'
        elif engine in ['cython', 'python']:
            if nThreads < -1 or nThreads == 0 or nThreads > mp.cpu_count():
                print 'Error: Parameter -n must be a number between 1 and %s for Cython/Python engines.' % str(mp.cpu_count())
                usage()
                sys.exit()
    # Parameter 'engine' was passed. Continue to next code block to set default nThreads parameter.
    except NameError:
        pass

    # Set some default parameters if not defined by the user
#    try:
#        verbose
#    except NameError:
#        verbose = False
    try:
        engine
    except NameError:
        engine = 'opencl-gpu'
    try:
        nThreads
    except NameError:
        if engine == 'cuda':
            nThreads = 32
        elif engine == 'opencl-gpu':
            nThreads = 32
        elif engine == 'opencl-cpu':
            nThreads = None
        elif engine in ['cython', 'python']:
            nThreads = -1
    
    # Determine correct computation engine.
    # Cascade in order of OpenCL-GPU -> OpenCL-CPU -> Cython -> Python
    if engine == 'cuda':
        try:
            import sobel_cuda as sobel
        except ImportError:
            print 'Warning: Could not load CUDA engine. Falling back to OpenCL engine with GPU support.'
            engine = 'opencl'
    if engine == 'opencl-gpu':
        try:
            import sobel_opencl as sobel
        except ImportError:
            print 'Warning: Could not load OpenCL GPU engine. Falling back to Cython engine.'
            engine = 'opencl-cpu'
            nThreads = None
    if engine == 'opencl-cpu':
        try:
            import sobel_opencl as sobel
        except ImportError:
            print 'Warning: Could not load OpenCL GPU engine. Falling back to Cython engine.'
            engine = 'cython'
            nThreads = -1
    if engine == 'cython':
        try:
            import sobel_c as sobel
        except ImportError:
            print 'Warning: Could not load Cython engine. Falling back to Python engine.'
            engine = 'python'
            try:
                nThreads
            except NameError:
                nThreads = -1
    if engine == 'python':
        import sobel_py as sobel
        try:
            nThreads
        except NameError:
            nThreads = -1
    
    # convert to number of detected cpu cores.
    if nThreads == -1:
        nThreads = mp.cpu_count()
    
    # Load image
    try:
        img = Image.open(fnameInput)
        img.load()
    except:
        print 'Error: Input file %s could not be loaded.' % fnameInput
        sys.exit()

    if img.mode != 'RGB':
        print 'Error: Input file is not in RGB format. Only RGB images are supported.'
        sys.exit()

    timeExecStart = time.time()

    # Transpose image if width > height ... Maximize number of threads & Reduce workload per thread
    if img.size[0] > img.size[1]:
        flagT = 1 # Transpose flag
        img = img.transpose(Image.ROTATE_90)
    else:
        flagT = 0

    # Get image dimensions (x, y)
    nPixelsX = img.size[1]
    nPixelsY = img.size[0]

    # Separate image into R, G, B matrices
    imgR, imgG, imgB = img.split()
    imgR = np.reshape(np.array(list(imgR.getdata())), (nPixelsX, nPixelsY))
    imgG = np.reshape(np.array(list(imgG.getdata())), (nPixelsX, nPixelsY))
    imgB = np.reshape(np.array(list(imgB.getdata())), (nPixelsX, nPixelsY))

    # Rescale RGB matrices from [0,255] to [0,1]
    imgR = imgR/255
    imgG = imgG/255
    imgB = imgB/255

    ## Begin sobel filtering
    imgR, timeKR = sobel.apply(imgR, nThreads)
    imgG, timeKG = sobel.apply(imgG, nThreads)
    imgB, timeKB = sobel.apply(imgB, nThreads)
    timeKernel = np.array([timeKR, timeKG, timeKB]).sum()   # Kernel times (Seconds)
        
    # Rescale image to [0, 255] and convert to PIL images
    imgR = np.round(imgR*255)
    imgG = np.round(imgG*255)
    imgB = np.round(imgB*255)
    imgR = Image.fromarray(imgR.astype(np.int32))
    imgG = Image.fromarray(imgG.astype(np.int32))
    imgB = Image.fromarray(imgB.astype(np.int32))
    imgR = imgR.convert('L')
    imgG = imgG.convert('L')
    imgB = imgB.convert('L')

    # Create new image & save
    # Initialize filtered image
    img = Image.merge('RGB', (imgR, imgG, imgB))

    # Rotate image if necessary
    if flagT == 1:
        img = img.transpose(Image.ROTATE_270)

    try:
        img.save(fnameOutput)
    except:
        print 'Warning: Could not save image as ' + fnameOutput
        fnameOutput = datetime.now().strftime('%Y-%m-%d_%H%M%S') + '.png'
        img.save(fnameOutput, 'png')
        print 'Saving image as ' + fnameOutput

    timeExecEnd = time.time()  # Execution Time (Seconds)
    timeExec = (timeExecEnd - timeExecStart)

    width = img.size[0]
    height = img.size[1]

    print 'Engine,Threads,Kernel Time,Execution Time,Width,Height'
    print engine + ',' + str(nThreads) + ',' + str(timeKernel) + ',' + str(timeExec) + \
        ',' + str(width) + ',' + str(height)

if __name__ == "__main__":
    mp.freeze_support() # Required for multiprocessing support on Windows
    main()
