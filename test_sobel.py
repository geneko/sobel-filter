from __future__ import division

import unittest

import numpy as np
import scipy.signal as spSignal
import scipy.misc as spMisc
   
class SobelTest(unittest.TestCase):
    """
    Test each Sobel implementation against SciPy's 2D convolution.
    
    The test image used is scipy.misc.ascent. The image is scaled to [0,1] and
    a Sobel filter is applied to it, and then rescaled back to [0, 255]
    
    The test will fail if any of the pixels do not match between each of our
    Sobel implementations and the SciPy 2D convolution.
    """    
    
    # Prevents overriding unittest.TestCase __init__
    def __init__(self, *args, **kwargs):
        super(SobelTest, self).__init__(*args, **kwargs)
        self.init_scipy()
    
    # Initialize by creating comparative Sobel filtered image using SciPy's 2D convolution 
    def init_scipy(self):
        # Import test image from SciPy and rescale to [0,1]
        self.testImage = spMisc.ascent().astype(np.float)/255
        
        # Define Sobel kernels
        sobelX = [[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]]
        sobelY = [[-1, -2, -1], [0, 0, 0], [1, 2, 1]]
        
        # Apply Sobel filter to test image by convolution
        gX = spSignal.convolve2d(self.testImage, sobelX, mode='same')
        gY = spSignal.convolve2d(self.testImage, sobelY, mode='same')
        imgF = np.sqrt((gX**2 + gY**2))
        
        # Rescale image to [0, 1], then back to [0, 255]
        imgF = (imgF - imgF.min())/(imgF.max() - imgF.min())
        self.testImageF = np.round(imgF*255).astype(np.int)
    
    # Compute error between SciPy image and sobel() image
    def abs_error(self, a):
        error = np.abs(a - self.testImageF).sum()
        return error    
    
    # Test NumPy single thread
    def test_numpy(self, nThreads=1):
        import sobel_py as sobel
    
        imgF = sobel.apply(self.testImage, nThreads)[0]
        imgF = np.round(imgF*255).astype(np.int)
        error = self.abs_error(imgF)
        self.failUnlessEqual(error, 0)

    # Test NumPy multiprocessing
    def test_numpy_mp(self, nThreads=2):
        self.test_numpy(nThreads)
    
    # Test Cython single thread
    def test_cython(self, nThreads=1):
        try:
            import sobel_c as sobel
        except ImportError:
            return unittest.SkipTest('Could not initialize Cython.')
    
        imgF = sobel.apply(self.testImage, nThreads)[0]
        imgF = np.round(imgF*255).astype(np.int)
        error = self.abs_error(imgF)
        self.failUnlessEqual(error, 0)
    
    # Test Cython mutltiprocessing
    def test_cython_mp(self, nThreads=2):
        self.test_cython(nThreads)

    # Test PyOpenCL on CPU
    def test_opencl_cpu(self, nThreads=None):
        try:
            import sobel_opencl as sobel
        except ImportError:
            if nThreads is None:
                return unittest.SkipTest('Could not initialize PyOpenCL on CPU.')
            else:
                return unittest.SkipTest('Could not initialize PyOpenCL on GPU.')
        
        imgF = sobel.apply(self.testImage, nThreads)[0]
        imgF = np.round(imgF*255).astype(np.int)
        error = self.abs_error(imgF)
        self.failUnlessEqual(error, 0)
        
    # Test PyOpenCL on GPU
    def test_opencl_gpu(self, nThreads=32):
        self.test_opencl_cpu(nThreads)
    
    # Test CUDA on NVIDIA GPU
    def test_cuda(self, nThreads=32):
        try:
            import sobel_cuda as sobel
        except ImportError:
            return unittest.SkipTest('Could not initialize CUDA.')
        
        imgF = sobel.apply(self.testImage, nThreads)[0]
        imgF = np.round(imgF*255).astype(np.int)
        error = self.abs_error(imgF)
        self.failUnlessEqual(error, 0)

if __name__ == '__main__':
    unittest.main()