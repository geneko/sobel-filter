# This file is used to build the Cython extensions using
# > python setup.py build_ext --inplace
#
# If you are on Windows and are having trouble compiling, see
# https://www.ibm.com/developerworks/community/blogs/jfp/entry/Installing_Cython_On_Anaconda_On_Windows?lang=en

from distutils.core import setup
from Cython.Build import cythonize
import numpy

#
setup(
  ext_modules = cythonize('sobel_c.pyx'),
  include_dirs=[numpy.get_include()],
)