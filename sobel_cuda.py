from __future__ import division # http://www.python.org/dev/peps/pep-0238/
import math
import time

import numpy as np

import pycuda.driver as cuda
from pycuda.elementwise import ElementwiseKernel
import pycuda.gpuarray as gpuarray
import pycuda.autoinit  # Initialize First CUDA Card
from pycuda.compiler import SourceModule

# CUDA Timers
cudaStart = cuda.Event()
cudaEnd = cuda.Event()

# C Cuda Kernels
cuda_kernel = """
__global__ void sobel(float *img, float *imgF, int nPixelsX, int nPixelsY)
{
    int idxX = blockIdx.x*blockDim.x + threadIdx.x; // Get row index (nPixelsX)
    int idxY;

    // Place Sobel kernels (rotated 180 degrees) in shared memory.
    __shared__ int s_kernelX[9];
    __shared__ int s_kernelY[9];

    s_kernelX[0] = 1;
    s_kernelX[1] = 0;
    s_kernelX[2] = -1;
    s_kernelX[3] = 2;
    s_kernelX[4] = 0;
    s_kernelX[5] = -2;
    s_kernelX[6] = 1;
    s_kernelX[7] = 0;
    s_kernelX[8] = -1;

    s_kernelY[0] = 1;
    s_kernelY[1] = 2;
    s_kernelY[2] = 1;
    s_kernelY[3] = 0;
    s_kernelY[4] = 0;
    s_kernelY[5] = 0;
    s_kernelY[6] = -1;
    s_kernelY[7] = -2;
    s_kernelY[8] = -1;
    __syncthreads();

    if(idxX < nPixelsX){ // Prevents last block from executing on an image that does not exist
        // Local Variables
        int posX;
        int posY;
        int i;
        int j;

        float gX;
        float gY;
        float neighbor[3*3]; // Image neighbor in local memory

        // Work across ypixels
        for(idxY = 0; idxY < nPixelsY; idxY++){
            // Extract image neighbor
            for(i = 0; i < 3; i++){
                for(j = 0; j < 3; j++){
                    posX = idxX - 1 + i;
                    posY = idxY - 1 + j;

                    if((posY >= 0) && (posY < nPixelsY) && (posX >= 0) && (posX < nPixelsX)){
                        neighbor[i*3 + j] = img[posX*nPixelsY + posY];
                    }
                    else{
                        neighbor[i*3 + j] = 0.;
                    }
                }
            }

            // Perform Sobel Filtering
            gX = 0.;
            gY = 0.;

            for(int i = 0; i < 9; i++){
                gX += s_kernelX[i]*neighbor[i];
                gY += s_kernelY[i]*neighbor[i];
            imgF[idxX*nPixelsY + idxY] = sqrt(gX*gX + gY*gY);
            }
        }
    }
}

"""
gpu = SourceModule(cuda_kernel)

# GPU scaling[0,1] kernel
gpu_scale = ElementwiseKernel(
    'float *a, float min, float max',
    'a[i] = (a[i] - min)/(max - min)',
    'scale')

def apply(img, nThreads):
    # Get image dimensions
    nPixelsX = np.int32(img.shape[0])
    nPixelsY = np.int32(img.shape[1])
    
    # Convert image to single precision
    img = np.float32(img)
    
    # Compute the number of CUDA blocks required
    nBlocks = np.int(math.ceil(nPixelsX/nThreads))

    # Transfer image matrices to GPU
    img_gpu = gpuarray.to_gpu(img)
    imgF_gpu = gpuarray.empty_like(img_gpu) # Filtered image
    
    # Bind gpu_sobel() to gpu kernel sobel()
    gpu_sobel = gpu.get_function('sobel')
    
    # Perform Sobel filtering on GPU and scale matrices
    cudaStart.record()
    gpu_sobel(img_gpu, imgF_gpu, nPixelsX, nPixelsY, block=(nThreads, 1, 1), grid=(nBlocks, 1))
    imgF_min = np.float32(gpuarray.min(imgF_gpu).get())
    imgF_max = np.float32(gpuarray.max(imgF_gpu).get())
    gpu_scale(imgF_gpu, imgF_min, imgF_max)
    cudaEnd.record()
    cudaEnd.synchronize()
    timeKernel = cudaStart.time_till(cudaEnd)*1e-3 # Kernel time in seconds
    
    # Transfer filtered image from GPU to CPU
    imgF = imgF_gpu.get()
    
    return imgF, timeKernel
