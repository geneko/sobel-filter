# High Performance Sobel Filter #
## About ##
This is the source code of an application that demonstrates the use of Python multiprocessing with shared memory, Cython, PyOpenCL, and PyCUDA to optimize existing Python code, which can result in tremendous speedup. Here, I developed a Sobel filter for image processing that will process color images in RGB format.

Details of this project, including benchmarks, can be found at http://geneko.bitbucket.org

## How to use? ##

This was written as a command-line utility. Simply executing the following command on *app.py* will reveal the parameters. The default parameters are:
> python app.py -i <input> -o <output>

By default, the Sobel filter will use the OpenCL engine on the CPU. OpenCL CPU runtime libraries for Windows and Linux are available from [AMD's APP SDK](http://developer.amd.com/tools-and-sdks/opencl-zone/amd-accelerated-parallel-processing-app-sdk/) for all x86 CPUs supporting at least the SSE2 instruction set, and from [Intel's OpenCL Runtimes](https://software.intel.com/en-us/articles/opencl-drivers) for Intel CPUs.

## Unit Tests ##
Unit tests of the sobel filters is provided in *test_sobel.py*. The test compares the output of a test image from each of the implemented Sobel filters against the output of the [SciPy 2D convolution function](http://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.convolve2d.html).

Executing the following command will start the unit tests.
> python test_sobel.py

## Dependencies ##
* Python 2
* NumPy
* Pillow
* Cython*
* PyCUDA*
* PyOpenCL*

*Optional

OpenCL Runtime Libraries

* [AMD APP SDK](http://developer.amd.com/tools-and-sdks/opencl-zone/amd-accelerated-parallel-processing-app-sdk/) (For all x86 CPUs)
* [Intel OpenCL Runtime](http://software.intel.com/en-us/articles/opencl-drivers) (For Intel CPUs)

## Cython Optimizations ##
To take advantage of Cython optimizations, be sure to build the C modules using the following command:
> python setup.py build_ext --inplace

## To Do ##
* Fix Cython C types on Linux/macOS