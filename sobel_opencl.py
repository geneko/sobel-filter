from __future__ import division # http://www.python.org/dev/peps/pep-0238/
import time

import numpy as np

try:
    import pyopencl as cl
    from pyopencl.elementwise import ElementwiseKernel
    from pyopencl import array as gpuarray
except:
    raise ImportError('Failed to load OpenCL library.')

opencl_kernel = """
__kernel void sobel(__global float *img, __global float *imgF, int nPixelsX, int nPixelsY)
{
    int idxX = get_global_id(0); // CUDA: idxX = blockIdx.x*blockDim.x + threadIdx.x;
    int idxY;

    // Place Sobel kernels (rotated 180 degrees) in local (shared) memory.
    __local int s_kernelX[9]; // CUDA: __shared__
    __local int s_kernelY[9];

    s_kernelX[0] = 1;
    s_kernelX[1] = 0;
    s_kernelX[2] = -1;
    s_kernelX[3] = 2;
    s_kernelX[4] = 0;
    s_kernelX[5] = -2;
    s_kernelX[6] = 1;
    s_kernelX[7] = 0;
    s_kernelX[8] = -1;

    s_kernelY[0] = 1;
    s_kernelY[1] = 2;
    s_kernelY[2] = 1;
    s_kernelY[3] = 0;
    s_kernelY[4] = 0;
    s_kernelY[5] = 0;
    s_kernelY[6] = -1;
    s_kernelY[7] = -2;
    s_kernelY[8] = -1;
    barrier(CLK_LOCAL_MEM_FENCE); // CUDA: __syncthreads(); 

    if(idxX < nPixelsX){ // Prevents last block from executing on an image that does not exist
        // Local Variables
        int posX;
        int posY;
        int i;
        int j;

        float gX;
        float gY;
        float neighbor[3*3]; // Image neighbor in local memory

        // Work across ypixels
        for(idxY = 0; idxY < nPixelsY; idxY++){
            // Extract image neighbor
            for(i = 0; i < 3; i++){
                for(j = 0; j < 3; j++){
                    posX = idxX - 1 + i;
                    posY = idxY - 1 + j;

                    if((posY >= 0) && (posY < nPixelsY) && (posX >= 0) && (posX < nPixelsX)){
                        neighbor[i*3 + j] = img[posX*nPixelsY + posY];
                    }
                    else{
                        neighbor[i*3 + j] = 0.;
                    }
                }
            }

            // Perform Sobel Filtering
            gX = 0.;
            gY = 0.;

            for(int i = 0; i < 9; i++){
                gX += s_kernelX[i]*neighbor[i];
                gY += s_kernelY[i]*neighbor[i];
            imgF[idxX*nPixelsY + idxY] = sqrt(gX*gX + gY*gY);
            }
        }
    }
}

"""

def apply(img, nThreads=None):
    # Attempt to detect first GPU/CPU device for OpenCL
    if nThreads == None:
        deviceType = 'CPU'
    else:
        deviceType = 'GPU'
    detectFlag = 0
    for detectPlatform in cl.get_platforms():
        for detectDevice in detectPlatform.get_devices():
            if cl.device_type.to_string(detectDevice.type) == deviceType:
                detectFlag = 1
                device = detectDevice
            if detectFlag == 1:
                break    
    if detectFlag == 0:
        raise ImportError('Could not detect an OpenCL compatible %s device.' %deviceType)
    
    print 'Performing OpenCL computation on ' + str(device)
    
    # Bind OpenCL device to context and create a command queue
    context = cl.Context([device])
    queue = cl.CommandQueue(context)
    
    # Compile OpenCL Sobel kernel and bind to gpu_sobel
    global opencl_kernel
    kernel = cl.Program(context, opencl_kernel).build()
    
    # Create an Elementwise Kernel for scaling matrix between [0,1]
    gpu_scale = ElementwiseKernel(context,
        '__global float *a, float min, float max',
        operation = 'a[i] = (a[i] - min)/(max - min)',
        name = 'scale')
    
    # Get image dimensions
    nPixelsX = np.int32(img.shape[0])
    nPixelsY = np.int32(img.shape[1])
    
    # Convert image to single precision
    img = np.float32(img)
    
    # Transfer image matrices to GPU
    img_gpu = gpuarray.to_device(queue, img)
    
    # Create an empty filtered matrix and transfer to GPU
    imgF = np.empty_like(img)
    imgF_gpu = gpuarray.to_device(queue, imgF)
    
    # Perform Sobel filtering on GPU and scale matrices
    timeKernelStart = time.time()
    #http://www.nehalemlabs.net/prototype/blog/2014/05/25/parallel-programming-with-opencl-and-python-vectors-and-concurrency/
    # cl.Program(queue, n_workers, n_local, params)
    # n_workers = total number of threads to launch
    # n_local = number of threads that will share a compute unit/shared memory (think threads/block in CUDA)
    # If using gpuarray, need to use .data to import the data into the kernel
    if nThreads is None:
        n_local = None
    else:
        n_local = (nThreads,1,1)
    gpuEvent = kernel.sobel(queue, (nPixelsX,1,1), n_local, img_gpu.data, imgF_gpu.data, nPixelsX, nPixelsY)
    gpuEvent.wait()
    imgF_min = gpuarray.min(imgF_gpu).get()
    imgF_max = gpuarray.max(imgF_gpu).get()
    gpuEvent = gpu_scale(imgF_gpu, imgF_min, imgF_max)
    gpuEvent.wait()
    
    timeKernelEnd = time.time()
    timeKernel = (timeKernelEnd - timeKernelStart)  # Seconds
        
    # Transfer filtered image from GPU to CPU
    imgF = imgF_gpu.get()
        
    return imgF, timeKernel
